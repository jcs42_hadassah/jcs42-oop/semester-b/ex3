Project #3- Exceptions
By:
Joey Mizrahi  
Tzion Kashani 

Description:
This program first takes a size of the function "map", after that you user enters from a selection of possible functions to either add/remove a function from the "map", resize it and calculate a specific x value for a function.  
*you may enter a file as an input as well.
*the more main focus is catching exceptions and errors (to stop it before possible crash) 
Design:
A main class which is calculator holds a vector of pointers to each given function. The father abstract class is Functions and it is the one that all other classes inherit from it.
Files:
main.cpp � calls the calculator with a type calculator object. 

Myexceptions.h:
holds all the exception classes

Utilities:
holds simple utility functions

Calculator.h
Takes the input and analysis it to send it to its appropriate function.

Calculator.cpp
It holds a vector that takes in what the user wants or creates what functions the user needs.

Function.h
Declares the given functions.

Function.cpp
Father class that sub classes inherit from.

VarX.cpp, Ln.cpp, Log.cpp, Mul.cpp, Add.cpp, Poly.cpp, Sqrt.cpp, Comp.cpp
The functions that are ran through the program and are held in a vector of functions that can be used in calculator class.

VarX.h, Ln.h, Log.h, Mul.h, Add.h, Poly.h, Sqrt.h, Comp.h
Declares all the functions that you can use in the program.

Data Structure:
Vector of pointers to functions.
Vector of strings of the possible options

Bugs:
No known bugs 


