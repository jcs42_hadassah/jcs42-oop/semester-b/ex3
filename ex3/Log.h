#pragma once
#include "UnaryFunc.h"
#include <math.h>
//===================//===================
class Log :
	public UnaryFunc
{
public:
	Log(const double &base,const std::shared_ptr<Function> &f);
	~Log();

private:
	//data members
	double m_base;


	//member functions
	inline std::string print(const std::string &s)const override;
	inline double eval(const double &x)const override;
};

