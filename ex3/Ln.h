#pragma once
#include "UnaryFunc.h"
class Ln :
	public UnaryFunc
{
public:
	Ln();
	~Ln();
private:
	inline std::string print(const std::string &s)const ;
	inline double eval(const double &x)const ;
};

