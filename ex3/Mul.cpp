#include "Mul.h"

//===================//===================//===================//===================//===================
Mul::Mul(const std::shared_ptr<Function> &f1, const std::shared_ptr<Function> &f2) :BinaryFunc(f1, f2) 
{
}
//===================
inline Mul::~Mul()
{
}
//===================//===================//===================
//polmorphic function
inline double Mul::eval(const double &x)const
{
	return m_func1->eval(x) * m_func2->eval(x);
}
//===================//===================//===================
//polymorphic function
inline std::string Mul::print(const std::string &s)const
{
	return (tmpPrint("*", s));
}

