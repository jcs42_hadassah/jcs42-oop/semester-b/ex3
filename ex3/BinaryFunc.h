#pragma once
#include "Function.h"
//abstract class that holds two pinters to other functions
class BinaryFunc :
	public Function
{
public:
	BinaryFunc(const std::shared_ptr<Function> &f1,const std::shared_ptr<Function> &f2);
	virtual ~BinaryFunc()=0;
protected:
	virtual std::string print(const std::string &s)const = 0;
	virtual double eval(const double &val)const=0;
	virtual inline std::string tmpPrint(const std::string & op,const std::string& s) const;

	std::shared_ptr <Function> m_func1;
	std::shared_ptr <Function> m_func2;

private:
};





