#include <cstdlib>
#include "Calculator.h"



int main()
{

	try
	{
		Calculator calculator;
		calculator.run();
	}
	catch (const std::exception &e)
	{
		print_with_color(e.what(),RED);
		exit(EXIT_FAILURE);
	}

	

	return EXIT_SUCCESS;
}