#include "Ln.h"
#include "VarX.h"
//===================//===================//===================
Ln::Ln() :UnaryFunc(std::make_shared<VarX>())
{
}
Ln::~Ln()
{
}
//===================//===================//===================
//polymorphic print function
inline std::string Ln::print(const std::string &s)const
{
	return ("Ln(" + m_func->print(s) + ")");
}
//===================//===================//===================
//polymorphic eval function
inline double Ln::eval(const double &x)const
{
	if(x > 0)
		return log(m_func->eval(x));
	throw(InvalidValueException());
}
