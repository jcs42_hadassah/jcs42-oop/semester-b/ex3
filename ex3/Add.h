#pragma once
#include "BinaryFunc.h"

class Add 
	:public BinaryFunc
{
public:
	Add(const std::shared_ptr<Function> &f1, const std::shared_ptr<Function> &f2);
	~Add();

private:
	//member functions
	inline double eval(const double &x)const override;
	inline std::string print(const std::string &s)const override;

};
