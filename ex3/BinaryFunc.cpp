#include "BinaryFunc.h"

//===================//===================//===================//===================//===================
BinaryFunc::BinaryFunc(const std::shared_ptr<Function> &f1,const std::shared_ptr<Function> &f2) 
	:m_func1(f1), m_func2(f2)
{
}
//===================//===================
BinaryFunc::~BinaryFunc()
{
}
//===================//===================//===================
//print helper function for binary operator functions (+,-,/,*)
std::string BinaryFunc::tmpPrint(const std::string & op, const std::string & s) const
{
	return("(" + m_func1->print(s) + ")" + op + "(" + m_func2->print(s) + ")");
}