#pragma once
#include "Function.h"
#include "Log.h"
#include "Ln.h"
#include "Poly.h"
#include "Sqrt.h"
#include "Add.h"
#include "Mul.h"
#include "Comp.h"

#include <sstream>
#include <fstream>
#include <algorithm>
#include <string>
#include <iomanip>
#include <cctype>

const int MIN_FUNCS = 2;
const int NUM_OF_OPTIONS = 13;
const int MAX_FILE_PATH = 200;

#define AND &&
#define OR ||


enum t_unary
{
	ADD_FUNC,
	MUL_FUNC,
	COMP_FUNC
};

class Calculator
{
public:
	void run();   //main run function
	
	Calculator();
	~Calculator();

private:
	//option functions
	void add   (std::string &s);
	void mul   (std::string &s);
	void comp  (std::string &s);
	void eval  (std::string &s);
	void poly  (std::string &s);
	void log   (std::string &s);
	void rev   (std::string &s);
	void read  (std::string &s);
	void del   (std::string &s);
	void resize(std::string &s);
	void help()const;
	void exitCalc()const;
	//invalid


	//option function helpers
	void make_unary(std::string &s, const t_unary&);  //function makes add,mul and comp functions

	void create_poly(std::stringstream &ss, const int size);  //helper function for creating polynomial

	bool resize_helper(std::string &s);  //checks if the given size is valid
	void set_size(const int new_size);   //does the actual resizing when its a smaller size than b4



	void get_max_funcs();   //asks user to enter max ammount of functions
	int get_actual_vec_size();

\
	void printFuncs()const;
	void set_strings();
	void handle_input();
	void handle_command(std::stringstream &ss);
	t_options get_enum(std::string &s);


	//validators
	inline bool in_vecRange(const int &index)const;
	inline bool enoughValues(const std::string &s)const;
	bool contains_alpha(const std::string &s) const;
	void send_exception(const std::string &s);
	void file_error(std::string &s);


	//
	inline void clear_screen()const;
	

	//data - members ---------------------
	std::vector<std::shared_ptr<Function>> m_functions;  //vector holds all funtions
	std::vector <std::string> m_possible_options;  //holds all possible strings

	int m_num_of_funcs_in_use;   //this is the actual amount of functions in use
	int m_max_num_of_funcs;   //this holds the users given input of max num of functions he would like


	bool reading_from_file = false;  //bolean to know if were reading from file or not

	std::ifstream m_file;  //the file we work with
	
};

