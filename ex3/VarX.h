#pragma once
#include "Function.h"

class VarX :
	public Function
{
public:
	VarX() = default;
	~VarX() = default;
private:
	inline std::string print(const std::string &s)const override { return s; }
	inline double eval(const double &x)const override { return x; }
};