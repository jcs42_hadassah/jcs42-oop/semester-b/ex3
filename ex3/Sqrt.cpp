#include "Sqrt.h"
#include "VarX.h"
//===================//===================//===================
Sqrt::Sqrt() :UnaryFunc( std::make_shared<VarX>())
{
}
//===================//===================//===================
Sqrt::~Sqrt()
{
}
//===================//===================//===================
//polymorphic print function
inline std::string Sqrt::print(const std::string &s)const
{
	return ("Sqrt(" + m_func->print(s) + ")");
}
//===================//===================//===================
//polmorphic eval function
inline double Sqrt::eval(const double &x)const
{
	if(x>=0)
		return sqrt(m_func->eval(x));
	throw(InvalidValueException());
}
