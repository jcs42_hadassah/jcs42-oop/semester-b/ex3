#pragma once
#include "Function.h"
//abstarct class
class UnaryFunc :
	public Function
{
public:
	UnaryFunc(const std::shared_ptr<Function> &f);
	UnaryFunc() {}
	virtual ~UnaryFunc()=0;
protected:
	virtual std::string print(const std::string &s)const = 0;
	virtual double eval(const double &val)const = 0;
	std::shared_ptr <Function> m_func;
private:
	
	
};

