#pragma once
#include "UnaryFunc.h"
class Sqrt :
	public UnaryFunc
{
public:
	Sqrt();
	~Sqrt();
private:
	inline std::string print(const std::string &s)const override;
	inline double eval(const double &x)const override;
};

