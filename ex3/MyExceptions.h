#pragma once
#include <exception>

//==============================================
class FileOpenException :public std::exception
{
public:
	FileOpenException() = default;
	~FileOpenException() = default;

	char const* what() const override { return "Error opening file\n"; }
};
//==============================================
class TooFewArgsException :public std::exception
{
public:
	TooFewArgsException() = default;
	~TooFewArgsException() = default;

	char const* what() const override { return "Not enough arguments \n"; }
};
//==============================================
class MinException :public std::exception
{
public:
	MinException() = default;
	~MinException() = default;

	char const* what() const override {return "Value entered too small \n"; }


};
//==============================================
class NoAlphaException :public std::exception
{
public:
	NoAlphaException() = default;
	~NoAlphaException() = default;

	char const* what() const override { return "Input entered must be all digits \n"; }
};
//==============================================
class InvalidIndexException :public std::exception
{
public:
	InvalidIndexException() = default;
	~InvalidIndexException() = default;

	char const* what() const override { return "Index given not in vector range \n"; }
};
//==============================================
class InvalidValueException :public std::exception
{
public:
	InvalidValueException() = default;
	~InvalidValueException() = default;

	char const* what() const override { return "Inavlid given value \n"; }
};

//==============================================
class InvalidLogBaseException :public std::exception
{
public:
	InvalidLogBaseException() = default;
	~InvalidLogBaseException() = default;

	char const* what() const override { return "Inavlid log base \n"; }
};

//==============================================
class RevException :public std::exception
{
public:
	RevException() = default;
	~RevException() = default;

	char const* what() const override { return "Cannot reverse this function \n"; }
};

//==============================================
class InvalidCommandException :public std::exception
{
public:
	InvalidCommandException() = default;
	~InvalidCommandException() = default;

	char const* what() const override { return "The command given does not exist \n"; }
};
//==============================================
//when user tries to create aother function but it exceeds max_funcs limit
class FuncLimitException :public std::exception
{
public:
	FuncLimitException() = default;
	~FuncLimitException() = default;

	char const* what() const override { return "Exceeding max amount of functions \n"; }
};
//==============================================
//when user tries to create aother function but it exceeds max_funcs limit
class CalculatorException :public std::exception
{
public:
	CalculatorException() = default;
	~CalculatorException() = default;

	char const* what() const override { return "Failed to run Calculator \n"; }
};

//==============================================
//when user tries to create aother function but it exceeds max_funcs limit
class WholeNumException :public std::exception
{
public:
	WholeNumException() = default;
	~WholeNumException() = default;

	char const* what() const override { return "Number given must be whole \n"; }
};


