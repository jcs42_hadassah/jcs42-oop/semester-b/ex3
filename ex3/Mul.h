#pragma once
#include "BinaryFunc.h"
class Mul
		:public BinaryFunc
	{
	public:
		Mul(const std::shared_ptr<Function> &f1,const std::shared_ptr<Function> &f2);
		~Mul();
	
	private:
		inline double eval(const double &x)const override;
		inline std::string print(const std::string &s)const override;
	};
