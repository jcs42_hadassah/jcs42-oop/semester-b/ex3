#pragma once
#include <memory>
#include <iostream>
#include <string>
#include "Utilities.h"
#include "MyExceptions.h"

//abstarct class
class Function
{
public:
	Function();

	virtual std::string print(const std::string &s)const = 0;
	virtual double eval(const double &val)const = 0;

	virtual ~Function() = 0 ;
protected:
};

