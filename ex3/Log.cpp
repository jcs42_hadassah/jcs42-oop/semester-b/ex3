#include "Log.h"
//===================//===================//===================
Log::Log(const double &base,  const std::shared_ptr<Function> &f)
	:UnaryFunc(f),
	m_base(base)
{
}
//===================
Log::~Log()
{
}
//===================//===================//===================//===================
inline std::string Log::print(const std::string &s)const
{
	return ("log"+to_string_with_precision(m_base,3)+ "("+ m_func->print(s) + ")");
}
//===================//===================//===================//===================
inline double Log::eval(const double &x)const
{
	if(x > 0)
		return (log10(m_func->eval(x)) / log10(m_base));
	throw(InvalidValueException());
}
