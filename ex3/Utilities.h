#pragma once
//holds helper functions
#include <sstream>
#include <iomanip>
#include <Windows.h>
//===================//===================//===================

#define MAX_STRING  5  //maximum size of string user can enter

#define WHITE	7
#define GRAY	8
#define GREEN  10
#define AQUA   11
#define RED	   12
#define PINK   13
#define YELLOW 14
//===================//===================//===================//===================
//function turns double into string with a precision of n after the point
inline std::string to_string_with_precision(const double  &a_value, const int &n = 6)
{
	std::ostringstream out;
	out << std::fixed << std::setprecision(n) << a_value;
	return out.str();
}
//===================//===================//===================
inline void print_with_color(const std::string &s,int color)
{
	HANDLE hConsole = GetStdHandle(STD_OUTPUT_HANDLE);
	SetConsoleTextAttribute(hConsole, color);
	std::cerr << s ;
	SetConsoleTextAttribute(hConsole, WHITE);
}
//===================//===================
enum t_options
{
	t_LOG,
	t_ADD,
	t_MUL,
	t_COMP,
	t_POLY,
	t_REV,
	t_EVAL,
	t_DEL,
	t_READ,
	t_RESIZE,
	
	t_HELP,
	t_EXIT,
	t_INVALID
};

