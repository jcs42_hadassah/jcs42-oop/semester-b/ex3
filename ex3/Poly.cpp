#include "Poly.h"
#include "VarX.h"


//=========================================================================================
Poly::Poly(const std::vector<double>& coeff) :m_coeffs(coeff),UnaryFunc(std::make_shared<VarX>())
{
	
}
//===================
Poly::Poly()
{
}
//===================//===================//===================
//function taked current vector of coeffs, copies to new one, then reverses that new one
//then it creates a new share_ptr of type poly and returns it
//inline std::shared_ptr<Poly> Poly::ReversePoly()const
//{
//	auto new_coeffs = m_coeffs;
//	
//	std::reverse(new_coeffs.begin(), new_coeffs.end());
//
//	return std::make_shared<Poly>(new_coeffs);
//}
//===================//===================//===================
//polymorphic print function
std::string Poly::print(const std::string &s)const
{
	std::string temp;
	int zero_counter = 0;;

	//if the given exp is zero then we return the zero polynomial
	if (!m_coeffs.size())
		return "0";
	for (int i = m_coeffs.size()-1; i >=0 ; i--)
	{
		auto coeffs = std::to_string(m_coeffs[i]);
		coeffs.erase(coeffs.find_last_not_of('0') + 1, std::string::npos);
		if (coeffs.back() == '.')
			coeffs.pop_back();

		if (m_coeffs[i] == 0)
		{
			zero_counter++;
			continue;  //we skip coeffeciants that are equal to zero
		}
		temp += coeffs+ "*(" + m_func->print(s) + ") ^" + std::to_string(i);
		//we dont want to print the plus sign towards the end
		if (i > 0)
			temp += " + ";
	}
	//if all coeffs are zero then we return the zero polynomial
	if (zero_counter == m_coeffs.size())
		return "0";
	return temp;
}
//===================//===================//===================
//polymorphic function
double Poly::eval(const double &val)const
{
	double total = 0;

	for (unsigned i = 0; i < m_coeffs.size(); i++)
	{
		total +=  (m_coeffs[i] * (pow(m_func->eval(val), i)));
	}
	return total;
}
//===================//===================//===================//===================
Poly::~Poly()
{
}
