#include "Comp.h"

//===================//===================//===================//===================//===================
Comp::Comp(const std::shared_ptr<Function> &f1,const std::shared_ptr<Function> &f2) :BinaryFunc(f1, f2) 
{
}
//===================
Comp::~Comp()
{
}
//===================//===================//===================
//polymorphic function
inline double Comp::eval(const double &x)const
{
	return (m_func1->eval(m_func2->eval(x)));
}
//===================//===================//===================
//polymorphic function
inline std::string Comp::print(const std::string &s)const
{
	return(m_func1->print(m_func2->print(s)));
}