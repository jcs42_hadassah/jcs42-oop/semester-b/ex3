#include "Add.h"
//===================//===================//===================//===================//===================

Add::Add(const std::shared_ptr<Function> &f1, const std::shared_ptr<Function> &f2) :BinaryFunc(f1, f2) 
{
}
//===================
Add::~Add()
{
}
//===================//===================//===================
//polymorphic print function
inline double Add::eval(const double &x)const
{
	return m_func1->eval(x) + m_func2->eval(x);
}
//===================//===================//===================
//polymorphic eval function
inline std::string Add::print(const std::string &s)const
{
	return(tmpPrint("+", s));
}
