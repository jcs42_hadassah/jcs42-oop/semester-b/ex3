#pragma once
#include "BinaryFunc.h"

class Comp
	:public BinaryFunc
{
public:
	Comp(const std::shared_ptr<Function> &f1,const std::shared_ptr<Function> &f2);
	~Comp();

private:
	inline double eval(const double &x)const override;
	inline std::string print(const std::string &s)const override;
};