
#include "Calculator.h"

//=========================
Calculator::Calculator() :m_num_of_funcs_in_use(MIN_FUNCS)
{
	//these are the default functions
	m_functions.push_back(std::make_shared<Ln>());
	m_functions.push_back(std::make_shared<Sqrt>());

	set_strings();   //setting vector that holds all option strings
}
//=======================
void Calculator::get_max_funcs()
{
	std::string s;

	while (true)
	{
		print_with_color("Enter max number of functions\n", GREEN);
		std::cin.exceptions(std::ios_base::failbit | std::ios_base::badbit);
		try
		{
			std::cin >> s;

			if (contains_alpha(s))		throw(NoAlphaException());

			double size_d = std::stod(s);
			if (floor(size_d) != size_d)  throw(WholeNumException());

			int size = (int)size_d;

			if (size < MIN_FUNCS)
				throw(MinException());

			else
			{
				m_max_num_of_funcs = size;
				return;
			}

		}
		catch (std::exception &e)
		{
			print_with_color(e.what(), RED);
			std::cin.clear();
			std::cin.ignore(100, '\n');
		}
	}
}
//===================//===================//===================//===================
inline bool Calculator::in_vecRange(const int &index)const
{
	return(index >= 0 AND index < (int)m_functions.size());
}
//===================//===================//===================//===================
//returns true if there is at least one alpha in string
bool Calculator::contains_alpha(const std::string &s) const
{
	if (s == "." OR s == "-")  //checking if the entire string is a "."
		return true;



	
	return !std::all_of(s.begin(), //returns true if one isnt either digit or .
		s.end(),
		[](char c) { return std::isdigit(c) OR c=='.' OR c =='-' ; });  //returns true if its a digit or a "."
}
//===================//===================//===================//===================
//checks if the fiven string isnt junk string
inline bool Calculator::enoughValues(const std::string &s)const
{
	if (s[0] == '\0')
		return false;
	return true;
}
//===================//===================//===================//===================
inline void Calculator::set_strings()
{
	m_possible_options.resize(NUM_OF_OPTIONS);

	m_possible_options[t_LOG] = "log";
	m_possible_options[t_ADD] = "add";
	m_possible_options[t_MUL] = "mul";
	m_possible_options[t_COMP] = "comp";
	m_possible_options[t_POLY] = "poly";
	m_possible_options[t_REV] = "rev";
	m_possible_options[t_DEL] = "del";
	m_possible_options[t_READ] = "read";
	m_possible_options[t_RESIZE] = "resize";
	m_possible_options[t_EVAL] = "eval";
	m_possible_options[t_HELP] = "help";
	m_possible_options[t_EXIT] = "exit";
	m_possible_options[t_INVALID] = "INVALID OPTION";
}
//================================================================
t_options Calculator::get_enum(std::string &s)
{
	for (unsigned i = 0; i < m_possible_options.size(); i++)
	{
		if (s == m_possible_options[i])
			return (t_options)i;
	}
	return t_INVALID;    //when the given string is not one of the options
}
//================================================================
void Calculator::handle_command(std::stringstream &ss)
{
	std::string command;
	std::string s = ss.str();

	//clear_screen();

	ss >> command;  //getting the first string of the line

	switch (get_enum(command))
	{
	case t_LOG:     log(s);				break;
	case t_ADD:     add(s);             break;
	case t_MUL:	    mul(s);		        break;
	case t_COMP:	comp(s);		    break;
	case t_REV:		rev(s);			    break;
	case t_POLY:    poly(s);            break;
	case t_EVAL:	eval(s);		    break;
	case t_DEL:		del(s);				break;
	case t_READ:	read(s);		    break;
	case t_RESIZE:  resize(s);          break;
	case t_HELP:    help();		        break;
	case t_EXIT:	exitCalc();				break;

	default:  //case invalid
		throw(InvalidCommandException());  
		break;
	}
}
//================================================================
void Calculator::handle_input()
{
	std::stringstream ss;
	std::string s;

	try
	{
		if (m_file.eof())
		{
			reading_from_file = false;
			m_file.close();
		}

		reading_from_file ? std::getline(m_file, s) : std::getline(std::cin, s);

		while (s.empty())  //keep asking for input while its empty
		{

			reading_from_file ? std::getline(m_file, s) : std::getline(std::cin, s);
		}

		ss << s;
		handle_command(ss);
	}
	catch (std::exception &e)
	{
		if (reading_from_file) {
			print_with_color(e.what(), RED);
			file_error(s);
		}
		else
			print_with_color(e.what(), RED);
	}
}
//=================================================
void Calculator::run()
{
	get_max_funcs();
	std::cin.clear();
	std::cin.ignore(100, '\n');
	printFuncs();

	while (true)
	{
		try
		{
			handle_input();
		}
		catch (const std::exception &e)
		{
			print_with_color(e.what(), RED);
		}
		printFuncs();
	}
}
//===================//===================//===================//===================
//goes through all the polymophic print functions
void Calculator::printFuncs()const
{
	std::string s = "Max functions: " + std::to_string(m_max_num_of_funcs) + "\n";
	std::cout << std::endl;
	print_with_color(std::string(30, '='), YELLOW);
	std::cout << std::endl;
	print_with_color(s, YELLOW);
	print_with_color("This is the function list:\n", GRAY);

	int i = 0;
	for (auto &e : m_functions)
	{
		if (e) {
			std::cout << i++ << ": ";
			std::cout << (e->print("x"));
			std::cout << std::endl;
		}
	}
	
	std::cout << std::endl;
	print_with_color("Please enter a command(\"help\" for command list) :\n", AQUA);
}
//===================//===================//===================//===================
//function called when there is error in function
void Calculator::file_error(std::string &s)
{
	print_with_color("Error in command:" + s + "\n", RED);
	

	while (true) {
		try
		{
			print_with_color("To continue to read from file press 1 to read from terminal press 2 \n", YELLOW);

			std::string user_choice;
			std::cin >> user_choice;

			if (!enoughValues(user_choice))			throw(TooFewArgsException());
			if (contains_alpha(user_choice))		throw(NoAlphaException());

			if (user_choice == "1") {
				reading_from_file = true;
				break;
			}
			else if (user_choice == "2")
			{
				reading_from_file = false;
				m_file.close();    //closing the file
				std::cin.clear();  //clearing the cin
				std::cin.ignore(100, '\n');  //clearing until end of line
				break;
			}
			else
				throw(InvalidCommandException());

		}
		catch (std::exception &e) {
			print_with_color(e.what(), RED);
		}
	}
}
//===================//===================//===================//===================
void Calculator::send_exception(const std::string &s)
{

	if (!enoughValues(s))			throw(TooFewArgsException());
	if (contains_alpha(s))			throw(NoAlphaException());

	double size_d = std::stod(s);
	if (floor(size_d) != size_d)	throw(WholeNumException());

	int index = std::stoi(s);
	if (!in_vecRange(index))		throw(InvalidIndexException());

	if (m_num_of_funcs_in_use >= m_max_num_of_funcs)
		throw(FuncLimitException());
}
//=====================================================================================
//function is called when user wants to create a unary function
void Calculator::make_unary(std::string &s, const  t_unary& f)
{
	std::stringstream ss(s);  //this holds entire line of input
	std::string command;
	std::string index1, index2;

	ss >> command;
	ss >> index1;
	ss >> index2;

	send_exception(index1);   //in here there are throws
	send_exception(index2);

	switch (f)  //switching on the functions wanted to be created
	{
	case ADD_FUNC:
		m_functions.push_back(std::make_shared<Add>(m_functions[std::stoi(index1)], m_functions[std::stoi(index2)]));
		break;
	case MUL_FUNC:
		m_functions.push_back(std::make_shared<Mul>(m_functions[std::stoi(index1)], m_functions[std::stoi(index2)]));
		break;
	case COMP_FUNC:
		m_functions.push_back(std::make_shared<Comp>(m_functions[std::stoi(index1)], m_functions[std::stoi(index2)]));
		break;
	default:
		break;
	}
	m_num_of_funcs_in_use++;  //if there is an eror we dont get here
}
//----------------------------------------------------------------------------------
void Calculator::add(std::string &s)
{
	make_unary(s, ADD_FUNC);
}
//----------------------------------------------------------------------------------
void Calculator::mul(std::string &s)
{
	make_unary(s, MUL_FUNC);
}
//----------------------------------------------------------------------------------
void Calculator::comp(std::string &s)
{
	make_unary(s, COMP_FUNC);
}
//----------------------------------------------------------------------------------
void Calculator::eval(std::string &s)
{
	std::stringstream ss(s);  //this holds entire line of input
	std::string command;   //dummy variable 
	std::string index, val;

	ss >> command;  //eating the command "eval"
	ss >> index;

	if (!enoughValues(index))			throw(TooFewArgsException());
	if (contains_alpha(index))			throw(NoAlphaException());

	double indexInt = std::stod(index);
	if (floor(indexInt) != indexInt)	throw(WholeNumException());

	indexInt = (int)indexInt;
	if (!in_vecRange(indexInt))			throw(InvalidIndexException());

	ss >> val;

	if (!enoughValues(val))				throw(TooFewArgsException());
	if (contains_alpha(val))			throw(NoAlphaException());

	double value = std::stod(val);

	std::cout << m_functions[indexInt]->print(std::to_string(value));
	std::cout << " = ";
	std::cout << to_string_with_precision(m_functions[indexInt]->eval(value), 2) << std::endl;;
}
//----------------------------------------------------------------------------------
void Calculator::poly(std::string &s)
{
	std::stringstream ss(s);  //this holds entire line of input
	std::string command;
	std::string str;
	
	ss >> command;

	ss >> str;  //gets the num of coeffs wanted by user 

	if (!enoughValues(str))			throw(TooFewArgsException());
	if (contains_alpha(str))		throw(NoAlphaException());

	double size_d = std::stod(str);

	//checking if given number is whole
	if (floor(size_d) != size_d)	throw(WholeNumException());

	int size = (int)size_d;

	if (size < 0)					throw(InvalidValueException());

	if (m_num_of_funcs_in_use >= m_max_num_of_funcs)    throw(FuncLimitException());

	create_poly(ss, size);
}
//----------------------------------------------------------------------------------
void Calculator::create_poly(std::stringstream &ss, const int size)
{
	std::vector<double> coeffs;

	for (int i = 0; i < size; i++)
	{
		std::string coeff;
		ss >> coeff;

		if (!enoughValues(coeff))
			throw(TooFewArgsException());

		if (contains_alpha(coeff))
			throw(NoAlphaException());

		double num;
		num = std::stod(coeff);
		coeffs.push_back(num);
	}
	m_functions.push_back(std::make_shared<Poly>(coeffs));
	m_num_of_funcs_in_use++;
}
//----------------------------------------------------------------------------------
void Calculator::rev(std::string &s)
{

	std::stringstream ss(s);  //this holds entire line of input
	std::string command;
	std::string str;

	ss >> command;

	ss >> str;

	send_exception(str);

	double index = std::stod(str);
	if (floor(index) != index)	throw(WholeNumException());
	index = (int)index;

	if (auto p = std::dynamic_pointer_cast<Poly>(m_functions[index]))
	{
		m_functions.push_back(p->ReversePoly());
		m_num_of_funcs_in_use++;
	}
	else
		throw(RevException());
}

//----------------------------------------------------------------------------------
void Calculator::log(std::string &s)
{
	std::stringstream ss(s);  //this holds entire line of input
	std::string command;
	std::string base, index;

	ss >> command;
	ss >> base;

	if (!enoughValues(base))
		throw(TooFewArgsException());
	if (contains_alpha(base))
		throw(NoAlphaException());

	double baseInt = std::stod(base);


	if (!((baseInt > 0 AND baseInt < 1) OR (baseInt > 1)))
		throw(InvalidLogBaseException());


	ss >> index;

	send_exception(index);

	double indexInt = std::stod(index);
	if (floor(indexInt) != indexInt)	throw(WholeNumException());

	indexInt = (int)indexInt;

	m_functions.push_back(std::make_shared<Log>(baseInt, m_functions[indexInt]));
	m_num_of_funcs_in_use++;
}
//----------------------------------------------------------------------------------
void Calculator::read(std::string &s)
{
	std::stringstream ss(s);  //this holds entire line of input
	std::string command;
	std::string file_name;

	ss >> command;  //dummy eating the "read" command

	try
	{
		ss >> file_name;

		if (!enoughValues(file_name))   throw(TooFewArgsException());


		m_file.open(file_name);  

		if (!m_file.is_open())          throw(FileOpenException());

		reading_from_file = true;  //we are now in reading file mode
	}
	catch (std::exception &e)  //when we dont succeedcto open file
	{
		print_with_color(e.what(), RED);
	}
}
//----------------------------------------------------------------------------------
void Calculator::del(std::string &s)
{
	std::stringstream ss(s);  //this holds entire line of input
	std::string command;   //dummy
	std::string str;

	ss >> command;  //getting the command "del"
	ss >> str;   //getting the wanted index to be deleted

	if (!enoughValues(str))      throw(TooFewArgsException());   //not enough values given
	if (contains_alpha(str))     throw(NoAlphaException());      //value given contains non digit character

	double index = std::stod(str);
	if (floor(index) != index)	throw(WholeNumException());

	index = (int)index;
	if (!in_vecRange(index))     throw(InvalidIndexException());   //index given not in range of vector

	m_functions.erase(m_functions.begin() + index);  //deleting the actual function
	m_num_of_funcs_in_use--; 

}
//----------------------------------------------------------------------------------
void Calculator::resize(std::string &s)
{
	std::stringstream ss(s);  //this holds entire line of input
	std::string command;   //dummy string
	std::string size_str;
	std::string user_choice;
	int size = 0;

	ss >> command;  //eats the command "resize"
	ss >> size_str;   //gets the wanted size from user
	
	if (resize_helper(size_str)) //when the size is smaller than the max size,and passed all validators
	{
		print_with_color("To change the vector size enter 1, for new size enter 2 \n", GREEN);
		std::cin >> user_choice;   //gets the users choice, 1 or 2

		if (user_choice == "1")
		{
			size = std::stoi(size_str);
			set_size(size);
		}
		else if (user_choice == "2")  //when user wants to change his mind for a new size
		{
			size_str.clear();       //clearing the last size 
			std::cin >> size_str;   //getting the users next choice for size
			
			if (resize_helper(size_str))   //when the size is smaller than the max size,and passed all validators
			{
				size = std::stoi(size_str);  //turning the users second choice of size into a number
				set_size(size);
			}
		}
		else  //when user doesnt enter 1 or 2
			throw(InvalidCommandException());
	}
}
//----------------------------------------------------------------------------------
//returns true when the size wanted is smaller than current, false othersize
//function also validates the size given from user
bool Calculator::resize_helper(std::string &s)
{
	if (!enoughValues(s))       throw(TooFewArgsException());
	if (contains_alpha(s))      throw(NoAlphaException());

	int size = std::stoi(s);
	if (size < 0)			    throw(MinException());   //invalid size

	else if (size >= m_max_num_of_funcs)  //if its bigger we just change
		m_max_num_of_funcs = size;

	else 
		return true;   //return true when its smaller

	return false;
}
//----------------------------------------------------------------------------------
//when we make the vector into a smaller size
void Calculator::set_size(const int new_size)
{
	m_functions.resize(new_size);
	m_max_num_of_funcs = new_size;
	m_num_of_funcs_in_use = get_actual_vec_size();
}
//----------------------------------------------------------------------------------
//returns size of cells in use
int Calculator:: get_actual_vec_size()
{
	int counter = 0;

	for (auto &e : m_functions)
	{
		if (e)
			counter++;
		else
			return counter;
	}
	return counter;
}
//===================//===================//===================//===================
//prints the help menu
void Calculator::help()const
{
	std::cout
		<< "eval(uate) num x - Evaluates function #num on x \n"
		<< "poly(nomial) N c0 c1 ... cN - 1 - Creates a polynomial with N coefficients \n"
		<< "rev(erse) num - Creates a polynomial with N coefficients in reverse order \n"
		<< "mul(tiply) num1 num2 - Creates a function that is the multiplication of function #num1 and function #num2 \n"
		<< "add num1 num2 - Creates a function that is the sum of function #num1 and function #num2 \n"
		<< "comp(osite) num1 num2 - Creates a function that is the composition of function #num1 and function #num2 \n"
		<< "log N num - Log N of function #num \n"
		<< "del(ete) num - Deletes function #num from function list \n"
		<< "help - Prints this help screen\n"
		<< "exit - Exits the program \n";
}
//================================================
void Calculator::exitCalc()const
{
	//clear_screen();
	std::cout << std::endl;
	print_with_color("Thank you for using Joey's and Tzion's Calculator!! \n", PINK); 
	Sleep(2500);  
	exit(EXIT_SUCCESS);
}
//================================================
void Calculator::clear_screen()const
{
#ifdef WIN32
	system("cls");
#else
	system("clear");
#endif
}
//================================================
Calculator::~Calculator()
{
}