#pragma once
#include "UnaryFunc.h"

#include <vector>

class Poly :
	public UnaryFunc
{
public:
	Poly(const std::vector<double> &coeffs);
	Poly();
	std::shared_ptr<Poly> ReversePoly()const 
	{
		auto new_coeffs = m_coeffs;

		std::reverse(new_coeffs.begin(), new_coeffs.end());

		return std::make_shared<Poly>(new_coeffs);
	}
	~Poly();
	
private:
	 std::string print(const std::string &s)const override;
	 double eval(const double &val)const override;

	std::vector<double> m_coeffs;
	
};


